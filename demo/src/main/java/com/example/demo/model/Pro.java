package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "pro")
public class Pro extends Client{

	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String refClient;
	private int siret;
	private String raisonSociale;
	private double ca;
	private Float kWhElec;
	private Float kWhGaz;
	
	
	
	public String getRefClient() {
		return refClient;
	}
	public void setRefClient(String refClient) {
		this.refClient = refClient;
	}
	public int getSiret() {
		return siret;
	}
	public void setSiret(int siret) {
		this.siret = siret;
	}
	public String getRaisonSociale() {
		return raisonSociale;
	}
	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}
	public double getCa() {
		return ca;
	}
	public void setCa(double ca) {
		this.ca = ca;
	}
	public Float getkWhElec() {
		return kWhElec;
	}
	public void setkWhElec(Float kWhElec) {
		this.kWhElec = kWhElec;
	}
	public Float getkWhGaz() {
		return kWhGaz;
	}
	public void setkWhGaz(Float kWhGaz) {
		this.kWhGaz = kWhGaz;
	}


	
}
