package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "particulier")
public class Particulier extends Client{
	
	@Id
	private String refClient;
	private String civilite;
	private String nom;
	private String prenom;
	private Float kWhElec;
	private Float kWhGaz;
	
	
//	public Particulier(String refClient, String civilite, String nom, String prenom) {
//		super();
//		this.refClient = refClient;
//		this.civilite = civilite;
//		this.nom = nom;
//		this.prenom = prenom;
//	}
	
	

	public String getRefClient() {
		return refClient;
	}


	public void setRefClient(String refClient) {
		this.refClient = refClient;
	}


	public String getCivilite() {
		return civilite;
	}


	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public Float getkWhElec() {
		return kWhElec;
	}


	public void setkWhElec(Float kWhElec) {
		this.kWhElec = kWhElec;
	}


	public Float getkWhGaz() {
		return kWhGaz;
	}


	public void setkWhGaz(Float kWhGaz) {
		this.kWhGaz = kWhGaz;
	}


	
	
	
	
	
}
