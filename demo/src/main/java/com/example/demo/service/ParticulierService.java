package com.example.demo.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Client;
import com.example.demo.model.Particulier;
import com.example.demo.model.Pro;
import com.example.demo.repository.ParticulierRepository;

@Service
public class ParticulierService {
	
	@Autowired
	ParticulierRepository particulierRepository;

	public List<Particulier> getAllParticulier() {
		// TODO Auto-generated method stub
		return particulierRepository.findAll();
	}

	public Particulier getParticulier(String refClient) {
		// TODO Auto-generated method stub
		return particulierRepository.findById(refClient);
	}

	public Particulier saveParticulier(Particulier particulier) {
		// TODO Auto-generated method stub
		if(particulier.getkWhElec() == null) {
			 particulier.setkWhElec(0.0f);
		}else if(particulier.getkWhGaz() == null) {
			particulier.setkWhGaz(0.0f);
		}
		Particulier saveParticulier = particulierRepository.save(particulier);
		return saveParticulier;
	}

	public void deleteParticulier(String refClient) {
		// TODO Auto-generated method stub
		particulierRepository.deleteById(refClient);
	}
	
	public Map<Enum, Double> getPrix(Particulier particulier) {
		double prix = 0.0;
		Map<Enum, Double> consommation = new HashMap<Enum, Double>();

		if(particulier != null) {
			
//			if(energie == Client.Energie.ELEC) {
				prix = 0.121 * particulier.getkWhElec();
				consommation.put(Client.Energie.ELEC, prix);
				
//			}else if(energie == Client.Energie.GAZ) {
				prix = 0.115 * particulier.getkWhGaz();
				consommation.put(Client.Energie.GAZ, prix);
//			}
			return consommation;
		}
//		return 0.0;// expect error
		return consommation;
	}


}
