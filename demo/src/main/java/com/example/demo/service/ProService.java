package com.example.demo.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Client;
import com.example.demo.model.Particulier;
import com.example.demo.model.Pro;
import com.example.demo.repository.ProRepository;

@Service
public class ProService {

	@Autowired
	ProRepository repository;
	
	public Pro getPro(final String refClient) {
        return repository.findByRef(refClient);
    }

	public List<Pro> getAllPro() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}
	
	public Pro savePro(Pro pro) {
		if(pro.getkWhElec() == null) {
			 pro.setkWhElec(0.0f);
		}else if(pro.getkWhGaz() == null) {
			pro.setkWhGaz(0.0f);
		}
		Pro savePro = repository.save(pro);
		return savePro;
	}

	public void deletePro(String refClient) {
		// TODO Auto-generated method stub
		repository.deleteById(refClient);
	}
	
	public Map<Enum, Double> getPrix(Pro pro) {
		double prix = 0.0;
		Map<Enum, Double> consommation = new HashMap<Enum, Double>();		
		if(pro.getCa() > 1000000) {
			prix = 0.114*pro.getkWhElec();
			consommation.put(Client.Energie.ELEC, prix);
			prix = 0.111*pro.getkWhGaz();
			consommation.put(Client.Energie.GAZ, prix);
		
		}else if(pro.getCa() < 1000000) {
			prix = 0.118*pro.getkWhElec();
			consommation.put(Client.Energie.ELEC, prix);
			prix = 0.113*pro.getkWhGaz();
			consommation.put(Client.Energie.GAZ, prix);
		}
		return consommation;
	}
			
	

	
}
