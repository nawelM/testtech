package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Pro;

@Repository
public interface ProRepository extends JpaRepository<Pro, Integer>{

	@Query(value = "SELECT * FROM pro WHERE pro.ref_Client = :refClient",nativeQuery = true)
	public Pro findByRef(@Param("refClient") String refClient);

	@Query(value = "DELETE FROM pro WHERE pro.ref_Client = :refClient",nativeQuery = true)
	public void deleteById(String refClient);

	

	
	
}
