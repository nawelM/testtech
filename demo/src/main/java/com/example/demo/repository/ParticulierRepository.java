package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Particulier;
import com.example.demo.model.Pro;

@Repository
public interface ParticulierRepository extends JpaRepository<Particulier, Integer>{

	@Query(value = "SELECT * FROM particulier WHERE particulier.ref_Client = :refClient",nativeQuery = true)
	Particulier findById(String refClient);
	
	@Query(value = "DELETE FROM particulier WHERE particulier.ref_Client = :refClient",nativeQuery = true)
	public void deleteById(String refClient);
}
