package com.example.demo.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Particulier;
import com.example.demo.model.Pro;
import com.example.demo.service.ProService;

@RestController
public class ProController {

	@Autowired
	ProService proService;
	
	@GetMapping("/pro")
	public List<Pro> getAllPro(){
		List<Pro> pro = proService.getAllPro();
		return pro;
	}
	
	@GetMapping("/pro/{refClient}")
	public Pro getPro(@PathVariable("refClient") final String refClient) {
		Pro pro = proService.getPro(refClient);
		if(pro != null) {
			return pro;
		}else{
			return null;
		}
	}
	
	@PostMapping("/addPro")
	public Pro addPro(@RequestBody Pro pro) {
		if(pro.getRefClient().startsWith("EKW") && pro.getRefClient().length() == 11) {
			return proService.savePro(pro);
		}
		// expected error
		return pro;
	}
	
	@DeleteMapping("/pro/{refClient}")
	public String deletePro(@PathVariable("refClient") final String refClient) {
		proService.deletePro(refClient);
		return "Deleted";
	}
	
	@GetMapping("/pro/{refClient}/facture")
	public Map<Enum, Double> getFactureByRef(@PathVariable("refClient") final String refClient) {
		Pro pro = proService.getPro(refClient);
		Map<Enum, Double> proFacture = proService.getPrix(pro);
		if(pro != null) {
			return proFacture;
		}else{
			return null;
		}
	}
	
}
