package com.example.demo.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Client;
import com.example.demo.model.Particulier;
import com.example.demo.model.Pro;
import com.example.demo.service.ParticulierService;
import com.example.demo.service.ProService;

@RestController
public class ParticulierController {

	@Autowired
	ParticulierService particulierService;
	
	@GetMapping("/particulier")
	public List<Particulier> getAllParticulier(){
		List<Particulier> particulier = particulierService.getAllParticulier();
		return particulier;
	}
	
	@GetMapping("/particulier/{refClient}")
	public Particulier getParticulier(@PathVariable("refClient") final String refClient) {
		Particulier particulier = particulierService.getParticulier(refClient);
		if(particulier != null) {
			return particulier;
		}else{
			return null;
		}
	}
	
	@PostMapping("/addParticulier")
	public Particulier addParticulier(@RequestBody Particulier particulier) {
		if(particulier.getRefClient().startsWith("EKW") && particulier.getRefClient().length() == 11) {
			return particulierService.saveParticulier(particulier);
		}
		return particulier;
	}
	
	@DeleteMapping("/particulier/{refClient}")
	public String deleteParticulier(@PathVariable("refClient") final String refClient) {
		particulierService.deleteParticulier(refClient);
		return "Deleted";
	}
	
	@GetMapping("/particulier/{refClient}/facture")
	public Map<Enum, Double> getFactureByRef(@PathVariable("refClient") final String refClient) {
		Particulier particulier = particulierService.getParticulier(refClient);
		Map<Enum, Double> proFacture = particulierService.getPrix(particulier);
		if(particulier != null) {
			return proFacture;
		}else{
			return null;
		}
	}
	
	
}
